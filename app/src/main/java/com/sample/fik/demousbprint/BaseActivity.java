package com.sample.fik.demousbprint;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Default on 27-Jan-16.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public byte[] SetTextSize(byte param) {
        byte[] cmd1 = new byte[3];
        cmd1[0] = 0x1b;
        cmd1[1] = 0x21;
        cmd1[2] = param;
        return cmd1;
    }

    public byte[] SetAlign(byte param) {
        byte[] cmd1 = new byte[3];
        cmd1[0] = 0x1b;
        cmd1[1] = 0x61;
        cmd1[2] = param;
        return cmd1;
    }

    public byte[] LineFeed() {
        byte[] cmd1 = new byte[1];
        cmd1[0] = 0x0A;
        return cmd1;
    }

    public byte[] setBarcodeHeight() {
        byte[] cmd1 = new byte[3];
        cmd1[0] = 0x1d;
        cmd1[1] = 0x68;
        cmd1[2] = 0x3c;
        return cmd1;
    }
    public byte[] setBarcodeWidth() {
        byte[] cmd1 = new byte[3];
        cmd1[0] = 0x1d;
        cmd1[1] = 0x77;
        cmd1[2] = 0x02;
        return cmd1;
    }
}
